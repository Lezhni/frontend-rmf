$ = jQuery;
var t, t1;

$(document).on('ready', ready);
$(document).on('scroll', scroll);
$(window).on('resize', resize);

// #header
$(document).on('mouseover', '.header-languages > ul > li', submenuShow);
$(document).on('mouseleave', '.header-languages > ul > li', submenuHide);
$(document).on('mouseover', '.child-menu', submenuOver);
$(document).on('mouseleave', '.child-menu', submenuLeave);

$(document).on('click', '.header-menu-mobile, body.mobile-panel-visible header, body.mobile-panel-visible main, body.mobile-panel-visible footer', toggleMobilePanel);
$(document).on('click', '.header-search input[type=submit]', toggleSearchBox);
$(document).on('click', '.header-contacts-popup, .header-info-close', toggleContacts);

// #catalog
$(document).on('click', '.catalog-style-type', toggleCatalogStyle);

// #tabs
$(document).on('click', '.tabs li a', toggleTab);

// #misc
$(document).on('click', '.index-slider-prev, .index-slider-next', changeMainSlide);
$(document).on('click', '.to-top', function() {

    $('body, html').animate({
        scrollTop: 0
    }, 1000);

});

function ready() {

    // #news
    if ($(window).innerWidth() > 1024) {

        $('.news-latest-single:not(.news-latest-big)').each(function() {
            $(this).find('.news-single-img img').innerHeight($(this).find('.news-single-content').innerHeight() + 30);
        });

        newsLatestHeight = $('.news-latest-single .news-inner').eq(1).height() + $('.news-latest-single .news-inner').eq(2).height() + 50;
       	if (($('.news-latest-big .news-single-img').height() + $('.news-latest-big .news-single-content').height()) < newsLatestHeight) {
        	$('.news-latest-big .news-inner').height(newsLatestHeight);
       	}
    }

    // #news grid
    if ($(window).innerWidth() > 768) {
        $('.news-archive').masonry({
            itemSelector: '.news-single'
        });
    }

    if ($('.index-page').length) {

        var firstScreenHeight = $('.index-first-screen').height();

        $('.index-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: false
        });

        changeHeader(firstScreenHeight);
        $(window).scroll(function() {

            changeHeader(firstScreenHeight);
        });

        $('.index-learn-more').click(function() {

            $('body, html').animate({
                scrollTop: firstScreenHeight
            }, 500);
        });
    }


    $('.product-gallery-single').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.product-gallery-small'
    });

    var productSlides = 8;
    var windowWidth = $(window).innerWidth();

    if (windowWidth > 1280 && windowWidth <= 1366) {
        var productSlides = 6;
    }
    if (windowWidth > 1034 && windowWidth <= 1280) {
        var productSlides = 4;
    }
    if (windowWidth <= 1024) {
        var productSlides = 2;
    }

    $('.product-gallery-small').slick({
        slidesToShow: productSlides,
        slidesToScroll: 1,
        asNavFor: '.product-gallery-single',
        dots: false,
        arrows: false,
        focusOnSelect: true
    });

}

function scroll() {}

function resize() {

    // #news
    if ($(window).innerWidth() > 1024) {

        $('.news-latest-single:not(.news-latest-big)').each(function() {
            $(this).find('.news-single-img img').innerHeight($(this).find('.news-single-content').innerHeight() + 30);
        });

        newsLatestHeight = $('.news-latest-single .news-inner').eq(1).height() + $('.news-latest-single .news-inner').eq(2).height() + 50;
       	if (($('.news-latest-big .news-single-img').height() + $('.news-latest-big .news-single-content').height()) < newsLatestHeight) {
        	$('.news-latest-big .news-inner').height(newsLatestHeight);
       	}
    }
}

function submenuShow() {

    clearTimeout(t);
    clearTimeout(t1);
    $(this).addClass('current');
}

function submenuHide() {

    t = setTimeout(function() {
        $('.header-languages > ul > li').removeClass("current");
    }, 200);
}

function submenuOver() {

    clearTimeout(t1);
    $(this).closest('li').addClass('current');
}

function submenuLeave() {

    t1 = setTimeout(function() {
        $('.header-languages > ul > li').removeClass("current");
    }, 200);
}

function toggleMobilePanel() {

    $('html, body').toggleClass('mobile-panel-visible');
}

function toggleSearchBox() {

    if ($(window).width() <= 751) {
        window.location.href = '/search';
        return false;
    }

    var searchInput = $(this).prev('input[type=text]').val();
    if (!searchInput || !searchInput.length || !searchInput.trim()) {
        $(this).closest('.header-search').toggleClass('header-search-open');
    } else {
        $(this).closest('.header-search').submit();
    }

    return false;
}

function toggleContacts() {

    $('.header-contacts').toggleClass('header-contacts-open');
}

function toggleCatalogStyle() {

    var catalogStyle = $(this).attr('data-style');
    $('.catalog-style-type').toggleClass('current');
    $('.catalog-goods').removeClass('catalog-cards catalog-list').addClass(catalogStyle);
}

function toggleTab() {

    var tabContainer = $(this).closest('.tabs').attr('data-for');
    var targetTab = $(this).attr('data-target');
    if (!$(this).closest('li').hasClass('current')) {
        $(this).closest('.tabs').find('li').toggleClass('current');
        $(tabContainer).find('.tab').removeClass('current');
        $(targetTab).addClass('current');
    }

    return false;
}

function changeHeader(firstScreenHeight) {

    if ($(window).scrollTop() >= firstScreenHeight) {
        $('header').removeClass('header-black header-transparent').addClass('header-white');
        $('.header-logo img').attr('src', '/img/logo-small-black.png');
    } else {
        $('header').removeClass('header-white').addClass('header-black header-transparent');
        $('.header-logo img').attr('src', '/img/logo-small-white.png');
    }
}

function changeMainSlide() {

    var target = $(this).attr('data-target');
    $('.index-slider').slick(target);

    return false;
}
